/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : localhost:3306
 Source Schema         : mc

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 02/06/2023 08:54:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for msg_company_wechat
-- ----------------------------
DROP TABLE IF EXISTS `msg_company_wechat`;
CREATE TABLE `msg_company_wechat`  (
  `msg_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '消息唯一id',
  `tip_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '提醒方式 0默认 1红点 2弹窗 3通知栏',
  `push_time_str` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '推送时间可读',
  `push_time` int(11) NOT NULL DEFAULT 0 COMMENT '推送时间',
  `msg_details_url` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息详情跳转路径',
  `receive_person` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息接收人',
  `msg_tag` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息标签',
  `msg_title` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息标题',
  `msg_content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容',
  PRIMARY KEY (`msg_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '企业微信消息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of msg_company_wechat
-- ----------------------------

-- ----------------------------
-- Table structure for msg_pool
-- ----------------------------
DROP TABLE IF EXISTS `msg_pool`;
CREATE TABLE `msg_pool`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `msg_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '消息唯一id',
  `msg_sub_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '消息业务唯一id',
  `msg_from` tinyint(4) NOT NULL DEFAULT 0 COMMENT '消息来源 1uc 2oa 3crm',
  `msg_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '消息类型 0默认 1待办 2预警 3通知 4日程',
  `msg_priority` tinyint(4) NOT NULL DEFAULT 0 COMMENT '消息优先级 0默认 2一般 4较优先 6优先',
  `push_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '推送类型 0实时推送 1定时推送',
  `channel_type` int(11) NOT NULL DEFAULT 0 COMMENT '渠道类型 三位格式 1xx站内信 2xx企业微信 3xx短信 4xx邮件 5xx微信公众号',
  `receive_man_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '消息接收id',
  `tip_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '提醒方式 0默认 1红点 2弹窗 3通知栏',
  `template_id` int(11) NOT NULL DEFAULT 0 COMMENT '消息模板id',
  `push_time_str` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '定时发送时间可读',
  `push_time` int(11) NOT NULL DEFAULT 0 COMMENT '发送时间',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间戳',
  `msg_details_url` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息详情跳转路径',
  `receive_person` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息接收人',
  `msg_tag` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息标签',
  `msg_title` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息标题',
  `msg_content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `msg_id`(`msg_id`) USING BTREE,
  INDEX `receive_man_id_type`(`receive_man_id`, `msg_type`) USING BTREE,
  INDEX `receive_man_id`(`receive_man_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 114201 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '消息记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of msg_pool
-- ----------------------------

-- ----------------------------
-- Table structure for msg_push_state
-- ----------------------------
DROP TABLE IF EXISTS `msg_push_state`;
CREATE TABLE `msg_push_state`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `msg_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '消息唯一id',
  `msg_sub_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '子消息唯一id',
  `msg_from` tinyint(4) NOT NULL DEFAULT 0 COMMENT '消息来源 1uc 2oa 3crm',
  `msg_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '消息类型 0默认 1待办 2预警 3通知 4日程',
  `msg_state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '消息状态 0未处理 1已处理',
  `read_state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '阅读状态 0未读 1已读',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0默认 1删除',
  `msg_priority` tinyint(4) NOT NULL DEFAULT 0 COMMENT '消息优先级 0默认 2一般 4较优先 6优先',
  `push_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '推送类型 0实时推送 1定时推送',
  `channel_type` int(11) NOT NULL DEFAULT 0 COMMENT '渠道类型 三位格式 1xx站内信 2xx企业微信 3xx短信 4xx邮件 5xx微信公众号',
  `receive_man_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '消息接收id',
  `tip_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '提醒方式 0默认 1红点 2弹窗 3通知栏',
  `err_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '错误状态 0正确 1错误',
  `cb_err_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '回调错误状态 0正确 1错误',
  `msg_template_id` int(11) NOT NULL DEFAULT 0 COMMENT '消息模板id',
  `err_code` tinyint(4) NOT NULL DEFAULT 0 COMMENT '错误代码',
  `cb_err_code` tinyint(4) NOT NULL DEFAULT 0 COMMENT '回调错误代码',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间戳',
  `push_time_str` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '推送时间可读',
  `push_time` int(11) NOT NULL DEFAULT 0 COMMENT '推送时间',
  `msg_details_url` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息详情跳转路径',
  `receive_person` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息接收人',
  `err_msg` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '错误原因',
  `cb_err_msg` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '回调错误原因',
  `msg_tag` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息标签',
  `msg_title` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息标题',
  `msg_content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `msg_id`(`msg_id`) USING BTREE,
  INDEX `receive_man_id_type`(`receive_man_id`, `msg_type`) USING BTREE,
  INDEX `receive_man_id`(`receive_man_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 114178 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '消息状态表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of msg_push_state
-- ----------------------------

-- ----------------------------
-- Table structure for msg_site
-- ----------------------------
DROP TABLE IF EXISTS `msg_site`;
CREATE TABLE `msg_site`  (
  `msg_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '消息唯一id',
  `tip_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '提醒方式 0默认 1红点 2弹窗 3通知栏',
  `push_time_str` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '推送时间可读',
  `push_time` int(11) NOT NULL DEFAULT 0 COMMENT '推送时间',
  `msg_details_url` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息详情跳转路径',
  `receive_person` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息接收人',
  `msg_tag` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息标签',
  `msg_title` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息标题',
  `msg_content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '消息内容',
  PRIMARY KEY (`msg_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '站内信消息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of msg_site
-- ----------------------------

-- ----------------------------
-- Table structure for msg_template
-- ----------------------------
DROP TABLE IF EXISTS `msg_template`;
CREATE TABLE `msg_template`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '模板唯一id',
  `tpl_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '模板id',
  `tpl_content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '模板内容',
  `add_time` int(11) NOT NULL DEFAULT 0 COMMENT '模板增加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '模板库' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of msg_template
-- ----------------------------
INSERT INTO `msg_template` VALUES (1, 'WX_1010001', '{\"title\":\"${first}\",\"count\":\"${key1}\",\"really\":\"${key2}\",\"reason\":\"${key3}\",\"result\":\"${remark}\"}', 0);
INSERT INTO `msg_template` VALUES (2, 'SITE_1010001', '{\"msgtype\":\"text\",\"text\":{\"content\":\"hello ${content}\"}}', 0);
INSERT INTO `msg_template` VALUES (3, 'SITE_1010002', '{\"title\":\"${title}\",\"content\":\"hello ${content}\"}', 0);
INSERT INTO `msg_template` VALUES (4, 'SITE_1010003', '{\"title\":\"${title}\",\"content\":\"hello ${content}\", \"cc\":\"hello ${cc}\"}', 0);
INSERT INTO `msg_template` VALUES (5, 'CW_1010001', '{\"msgtype\":\"text\",\"text\":{\"content\":\"注意： ${content}\"}}', 0);

SET FOREIGN_KEY_CHECKS = 1;
