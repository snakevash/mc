package org.msgcenter.mc;

import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONUtil;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.junit5.Timeout;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.msgcenter.mc.actor.MsgRedisActor;
import org.msgcenter.mc.common.JsonResult;
import org.msgcenter.mc.ds.SiteMsg;

import java.util.concurrent.TimeUnit;

@ExtendWith(VertxExtension.class)
@Slf4j
public class TestMsgRedisActor {

  private String deployId;

  @BeforeEach
  @DisplayName("")
  void prepare(Vertx vertx, VertxTestContext context) {
    vertx.deployVerticle(new MsgRedisActor(), new DeploymentOptions())
      .onSuccess(id -> {
        deployId = id;
        context.completeNow();
      });
  }

  private SiteMsg createSiteMsgRandom() {
    SiteMsg msg = new SiteMsg();

    msg.setMsgId(IdUtil.getSnowflakeNextId());
    // msg.setMsgId(1642138861828960256L);

    return msg;
  }

  @Test
  @Timeout(value = 10, timeUnit = TimeUnit.SECONDS)
  void testOnSiteMsgIdCheck(Vertx vertx, VertxTestContext context) {
    EventBus bus = vertx.eventBus();
    SiteMsg msg = createSiteMsgRandom();

    bus.request("site_msg.msg_id.check", JSONUtil.toJsonStr(msg))
      .onSuccess(res -> {
        JsonResult<String, Boolean> r = JSONUtil.toBean(res.body().toString(), new TypeReference<>() {
        }, true);

        log.info("测试站点消息id重复校验: {}", r);
        context.verify(() -> {
          Assertions.assertTrue(r.isOk());
          Assertions.assertTrue(r.getData());
        });
        context.completeNow();
      });


  }

  @AfterEach
  void clear(Vertx vertx, VertxTestContext context) {
    vertx.undeploy(deployId).onSuccess(res -> context.completeNow());
  }
}
