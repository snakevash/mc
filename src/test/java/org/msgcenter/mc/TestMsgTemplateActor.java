package org.msgcenter.mc;

import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import io.vertx.core.CompositeFuture;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.junit5.Timeout;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.msgcenter.mc.actor.MsgDbActor;
import org.msgcenter.mc.actor.MsgRedisActor;
import org.msgcenter.mc.actor.MsgTemplateActor;
import org.msgcenter.mc.common.JsonResult;
import org.msgcenter.mc.ds.TplRequestMsg;

import java.util.concurrent.TimeUnit;

@ExtendWith(VertxExtension.class)
@Slf4j
public class TestMsgTemplateActor {

  private String deployId;
  private String deployId2;
  private String deployId3;

  @BeforeEach
  @DisplayName("测试模板功能")
  void prepare(Vertx vertx, VertxTestContext context) {
    Future<String> d1 = vertx
      .deployVerticle(new MsgRedisActor(), new DeploymentOptions())
      .onSuccess(id -> deployId = id);

    Future<String> d2 = vertx
      .deployVerticle(new MsgTemplateActor(), new DeploymentOptions())
      .onSuccess(id -> deployId2 = id);

    Future<String> d3 = vertx
      .deployVerticle(new MsgDbActor(), new DeploymentOptions())
      .onSuccess(id -> deployId3 = id);

    CompositeFuture.all(d1, d2, d3)
      .onSuccess(res -> context.completeNow());
  }

  private String createTplMsg() {
    return "{\"first\":\"考勤月度报告\",\"key1\":\"应到22天\",\"key2\":\"实到21天\",\"key3\":\"请假1天\",\"remark\":\"优\"}";
  }

  @Test
  @Timeout(value = 20, timeUnit = TimeUnit.SECONDS)
  @DisplayName("模板渲染测试")
  void testOnRender(Vertx vertx, VertxTestContext context) {
    EventBus eb = vertx.eventBus();
    TplRequestMsg requestMsg = new TplRequestMsg(1, "WX_1010001", createTplMsg());

    eb.request("service.tpl.render", JSONUtil.toJsonStr(requestMsg))
      .onSuccess(payload -> {
        JsonResult<String, String> jr = JSONUtil.toBean(payload.body().toString(), new TypeReference<>() {
        }, true);

        context.verify(() -> {
          Assertions.assertTrue(jr.isOk());
          Assertions.assertTrue(StrUtil.isNotBlank(jr.getData()));
          log.info("模板渲染结果: {}", jr.getData());
        });

        context.completeNow();
      })
      .onFailure(err -> {
        log.error("模板测试异常 {}", err.getMessage());
        context.completeNow();
      });
  }

  @AfterEach
  void clear(Vertx vertx, VertxTestContext context) {
    Future<Void> f1 = vertx.undeploy(deployId);
    Future<Void> f2 = vertx.undeploy(deployId2);
    Future<Void> f3 = vertx.undeploy(deployId3);
    CompositeFuture.all(f1, f2, f3).onSuccess(r -> context.completeNow());
  }
}
