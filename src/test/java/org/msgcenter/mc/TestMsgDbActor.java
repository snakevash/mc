package org.msgcenter.mc;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.msgcenter.mc.actor.MsgDbActor;
import org.msgcenter.mc.ds.RequestMsg;

@ExtendWith(VertxExtension.class)
@Slf4j
public class TestMsgDbActor {

  private EventBus eb;

  private String deployId;

  @BeforeEach
  @DisplayName("测试数据库dbActor操作")
  void prepare(Vertx vertx, VertxTestContext context) {
    eb = vertx.eventBus();

    vertx.deployVerticle(new MsgDbActor(), context.succeeding(id -> {
      deployId = id;
      log.info("部署入库组件成功: {}", id);
      context.completeNow();
    }));
  }

  private RequestMsg createRequestMsgRandom() {
    RequestMsg msg = new RequestMsg();
    msg.setMsgId(IdUtil.getSnowflakeNextId());
    msg.setMsgSubId(IdUtil.getSnowflakeNextId());

    Byte[] from = {1, 2, 3};
    msg.setMsgFrom(RandomUtil.randomEle(from));

    Byte[] msgType = {0, 1, 2, 3, 4};
    msg.setMsgType(RandomUtil.randomEle(msgType));

    Byte[] priority = {0, 2, 4, 6};
    msg.setMsgPriority(RandomUtil.randomEle(priority));

    Byte[] pushType = {0, 1};
    msg.setPushType(RandomUtil.randomEle(pushType));

    Integer[] channelType = {101, 201, 301, 401, 501};
    msg.setChannelType(RandomUtil.randomEle(channelType));

    msg.setReceiveManId(RandomUtil.randomString(6));

    Byte[] tipType = {0, 1, 2, 3};
    msg.setTipType(RandomUtil.randomEle(tipType));

    msg.setTemplateId(0);

    msg.setPushTimeStr(DateUtil.now());
    msg.setPushTime(Convert.toInt(System.currentTimeMillis()));
    msg.setTs(LocalDateTimeUtil.now());
    msg.setMsgDetailsUrl(RandomUtil.randomString(66));
    msg.setReceivePerson(RandomUtil.randomString(8));
    msg.setMsgTag(RandomUtil.randomString(4));
    msg.setMsgTitle(RandomUtil.randomString(5));
    msg.setMsgContent(RandomUtil.randomString(255));

    return msg;
  }

  @Test
  void testOnAddRequestMsg(Vertx vertx, VertxTestContext context) {
    RequestMsg msg = createRequestMsgRandom();

    eb.request("db.post.add_request_msg", JSONUtil.toJsonStr(msg))
      .onSuccess(res -> {
        log.info("请求消息入库结果： {}", res.body());
        context.verify(() -> Assertions.assertTrue(ObjectUtil.isNotNull(res.body())));
        context.completeNow();
      });
  }

  @AfterEach
  void clear(Vertx vertx, VertxTestContext context) {
    vertx.undeploy(deployId)
      .onSuccess(res -> {
        log.info("卸载入库组件成功 {}", res);
        context.completeNow();
      });
  }
}
