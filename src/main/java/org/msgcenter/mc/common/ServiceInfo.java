package org.msgcenter.mc.common;


import lombok.Data;

@Data
public class ServiceInfo<T, R> {
  // 服务名称
  private String name;

  // 传入数据结构
  private T req;

  // 传出数据结构
  private R resp;

  // 服务类别
  private String type;
}
