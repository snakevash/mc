package org.msgcenter.mc.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import io.vertx.sqlclient.Tuple;
import org.msgcenter.mc.ds.*;

import java.util.List;


public class ConvertUtil {

  public static Tuple requestMsgToTuple(RequestMsg msg) {
    // (`msg_id`, `msg_sub_id`, `msg_from`, `msg_type`, `msg_priority`, `push_type`, `channel_type`, `receive_man_id`,
    // `tip_type`, `template_id`, `push_time_str`, `push_time`, `msg_details_url`, `receive_person`, `msg_tag`, `msg_title`, `msg_content`)
    return Tuple.of(msg.getMsgId(), msg.getMsgSubId(), msg.getMsgFrom(), msg.getMsgType(), msg.getMsgPriority(), msg.getPushType(), msg.getChannelType(), msg.getReceiveManId(),
      msg.getTipType(), msg.getTemplateId(), msg.getPushTimeStr(), msg.getPushTime(), msg.getMsgDetailsUrl(), msg.getReceivePerson(), msg.getMsgTag(), msg.getMsgTitle(), msg.getMsgContent());
  }

  public static Tuple siteMsgToTuple(SiteMsg msg) {
    // (`msg_id`, `tip_type`, `push_time_str`, `push_time`, `msg_details_url`, `receive_person`, `msg_tag`, `msg_title`, `msg_content`)
    return Tuple.from(List.of(msg.getMsgId(), msg.getTipType(), msg.getPushTimeStr(), msg.getPushTime(),
      msg.getMsgDetailsUrl(), msg.getReceivePerson(), msg.getMsgTag(), msg.getMsgTitle(), msg.getMsgContent()));
  }

  public static Tuple companyWechatMsgToTuple(CompanyWechatMsg msg) {
    // (`msg_id`, `tip_type`, `push_time_str`, `push_time`, `msg_details_url`, `receive_person`, `msg_tag`, `msg_title`, `msg_content`)
    return Tuple.from(List.of(msg.getMsgId(), msg.getTipType(), msg.getPushTimeStr(), msg.getPushTime(),
      msg.getMsgDetailsUrl(), msg.getReceivePerson(), msg.getMsgTag(), msg.getMsgTitle(), msg.getMsgContent()));
  }

  public static Tuple pushStateToTuple(PushStateMsg msg) {
    // (`msg_id`, `msg_sub_id`, `msg_from`, `msg_type`, `msg_state`, `read_state`, `is_deleted`, `msg_priority`, `push_type`,
    // `channel_type`, `receive_man_id`, `tip_type`, `err_type`, `cb_err_type`, `msg_template_id`, `err_code`, `cb_err_code`,
    // `push_time_str`, `push_time`, `msg_details_url`, `receive_person`, `err_msg`, `cb_err_msg`, `msg_tag`, `msg_title`, `msg_content`)
    return Tuple.of(msg.getMsgId(), msg.getMsgSubId(), msg.getMsgFrom(), msg.getMsgType(), msg.getMsgState(), msg.getReadState(), msg.getIsDelete(), msg.getMsgPriority(), msg.getPushType(),
      msg.getChannelType(), msg.getReceiveManId(), msg.getTipType(), msg.getErrType(), msg.getCbErrType(), msg.getMsgTemplateId(), msg.getErrCode(), msg.getCbErrCode(),
      msg.getPushTimeStr(), msg.getPushTime(), msg.getMsgDetailsUrl(), msg.getReceivePerson(), msg.getErrMsg(), msg.getCbErrMsg(), msg.getMsgTag(), msg.getMsgTitle(), msg.getMsgContent());
  }

  public static Tuple updatePushStateToTuple(PushStateMsg msg) {
    // SET msg_state = ?, push_time = ?, push_time_str = ? , err_type = ?, err_code = ?, err_msg = ? WHERE msg_id = ?
    return Tuple.of(msg.getMsgState(), msg.getPushTime(), msg.getPushTimeStr(), msg.getErrType(), msg.getErrCode(), msg.getErrMsg(), msg.getMsgId());
  }

  public static PushStateMsg siteMsgToPushStateMsg(SiteMsg msg) {
    PushStateMsg stateMsg = new PushStateMsg();

    BeanUtil.copyProperties(msg, stateMsg, false);

    return stateMsg;
  }

  public static PushStateMsg siteMsgToPushStateMsg(CompanyWechatMsg msg) {
    PushStateMsg stateMsg = new PushStateMsg();

    BeanUtil.copyProperties(msg, stateMsg, false);

    return stateMsg;
  }

  public static PushStateMsg requestMsgToPushStateMsg(RequestMsg msg) {
    PushStateMsg stateMsg = new PushStateMsg();

    BeanUtil.copyProperties(msg, stateMsg, false);
    stateMsg.setMsgState((byte) 0);
    stateMsg.setReadState((byte) 0);
    stateMsg.setIsDelete((byte) 0);
    stateMsg.setCbErrType((byte) 0);
    stateMsg.setPushTime((int) (System.currentTimeMillis() / 1000));
    stateMsg.setPushTimeStr(DateUtil.now());
    stateMsg.setCbErrMsg("");
    stateMsg.setCbErrCode((byte) 0);
    stateMsg.setErrType((byte) 0);
    stateMsg.setErrCode((byte) 0);
    stateMsg.setErrMsg("");
    stateMsg.setMsgTemplateId(msg.getTemplateId());

    return stateMsg;
  }

  public static RequestMsg reqSiteMsgToRequestMsg(ReqSiteMsg req) {
    RequestMsg msg = new RequestMsg();

    // 雪花id
    msg.setMsgId(IdUtil.getSnowflake().nextId());
    msg.setMsgSubId(IdUtil.getSnowflake().nextId());
    BeanUtil.copyProperties(req, msg, false);

    return msg;
  }

  public static RequestMsg companyWechatMsgToRequestMsg(ReqCompanyWechatMsg req) {
    RequestMsg msg = new RequestMsg();

    // 雪花id
    msg.setMsgId(IdUtil.getSnowflake().nextId());
    msg.setMsgSubId(IdUtil.getSnowflake().nextId());
    BeanUtil.copyProperties(req, msg, false);

    return msg;
  }

  public static ReqSiteMsg requestMsgToReqSiteMsg(RequestMsg req) {
    ReqSiteMsg msg = new ReqSiteMsg();

    BeanUtil.copyProperties(req, msg);

    return msg;
  }

  public static SiteMsg requestMsgToSiteMsg(RequestMsg req) {
    SiteMsg msg = new SiteMsg();

    BeanUtil.copyProperties(req, msg);

    return msg;
  }

  public static CompanyWechatMsg requestMsgToCompanyWechatMsg(RequestMsg req) {
    CompanyWechatMsg cwm = new CompanyWechatMsg();

    BeanUtil.copyProperties(req, cwm);

    return cwm;
  }
}
