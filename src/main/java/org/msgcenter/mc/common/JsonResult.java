package org.msgcenter.mc.common;

import cn.hutool.http.HttpStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JsonResult<T, R> {

  // 结果 1 正确 0 不正确
  private Integer status;

  // 消息
  private T msg;

  // 数据
  private R data;

  // 是否正确
  public boolean isOk() {
    return status == HttpStatus.HTTP_OK;
  }
}
