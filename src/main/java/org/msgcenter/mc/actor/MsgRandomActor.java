package org.msgcenter.mc.actor;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import lombok.extern.slf4j.Slf4j;
import net.datafaker.Faker;
import org.msgcenter.mc.ds.ReqCompanyWechatMsg;
import org.msgcenter.mc.ds.ReqSiteMsg;
import org.msgcenter.mc.ds.SendMsgHelpers;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

@Slf4j
public class MsgRandomActor extends AbstractVerticle {

  private final Faker faker = new Faker(new Locale("zh-CN"));

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    super.start(startPromise);

    Router router = Router.router(vertx);

    onCreateSiteMsgRandom(router);
    onCreateCwMsgRandom(router);

    vertx.createHttpServer()
      .requestHandler(router)
      .listen(8881)
      .onSuccess(http -> log.info("随机消息http服务启动成功 监听8881端口"))
      .onFailure(err -> log.error("随机消息http服务启动失败 {}", err.getMessage()));
  }

  private void onCreateCwMsgRandom(Router router) {
    Route route = router.get("/random/cwMsg");

    route.handler(rc -> {
      ReqCompanyWechatMsg msg = new ReqCompanyWechatMsg();

      msg.setMsgFrom(RandomUtil.randomEle(SendMsgHelpers.msgFromRange));
      msg.setMsgType(RandomUtil.randomEle(SendMsgHelpers.msgTypeRange));
      msg.setMsgPriority(RandomUtil.randomEle(SendMsgHelpers.msgPriorityRange));
      msg.setPushType(RandomUtil.randomEle(SendMsgHelpers.pushTypeRange));
      msg.setReceiveManId(RandomUtil.randomString(16));
      msg.setTipType(RandomUtil.randomEle(SendMsgHelpers.tipTypeRange));

      // 随机模板
      LocalDateTime now = LocalDateTimeUtil.now();
      LocalDateTime targetDateTime = LocalDateTimeUtil.offset(now, RandomUtil.randomInt(10, 200), ChronoUnit.SECONDS);

      msg.setPushTimeStr(targetDateTime.format(DatePattern.NORM_DATETIME_FORMATTER));
      msg.setPushTime((int) targetDateTime.toInstant(ZoneOffset.of("+8")).getEpochSecond());

      String rurl = StrUtil.format("https://www.baidu.com/search?key={}-{}-{}", RandomUtil.randomString(3), RandomUtil.randomString(3), RandomUtil.randomString(3));
      msg.setMsgDetailsUrl(rurl);

      msg.setReceivePerson(faker.name().name());
      msg.setMsgTag(faker.job().keySkills());
      msg.setMsgTitle(faker.weather().description());

      msg.setTemplateId(RandomUtil.randomEle(SendMsgHelpers.cwTplRange).getId());
      if (msg.getTemplateId() == 5) {
        msg.setMsgContent(StrUtil.format("{\"content\":\"{}\"}", faker.house().room()));
      }


      rc.response().putHeader("content-type", "application/json;charset=utf-8");
      rc.end(JSONUtil.toJsonStr(msg));
    });
  }

  // 站点消息随机生成器
  private void onCreateSiteMsgRandom(Router router) {
    Route route = router.get("/random/siteMsg");

    route.handler(rc -> {
      ReqSiteMsg msg = new ReqSiteMsg();

      msg.setMsgFrom(RandomUtil.randomEle(SendMsgHelpers.msgFromRange));
      msg.setMsgType(RandomUtil.randomEle(SendMsgHelpers.msgTypeRange));
      msg.setMsgPriority(RandomUtil.randomEle(SendMsgHelpers.msgPriorityRange));
      msg.setPushType(RandomUtil.randomEle(SendMsgHelpers.pushTypeRange));
      msg.setReceiveManId(RandomUtil.randomString(16));
      msg.setTipType(RandomUtil.randomEle(SendMsgHelpers.tipTypeRange));

      // 随机模板

      LocalDateTime now = LocalDateTimeUtil.now();
      LocalDateTime targetDateTime = LocalDateTimeUtil.offset(now, RandomUtil.randomInt(10, 200), ChronoUnit.SECONDS);

      msg.setPushTimeStr(targetDateTime.format(DatePattern.NORM_DATETIME_FORMATTER));
      msg.setPushTime((int) targetDateTime.toInstant(ZoneOffset.of("+8")).getEpochSecond());

      String rurl = StrUtil.format("https://www.baidu.com/search?key={}-{}-{}", RandomUtil.randomString(3), RandomUtil.randomString(3), RandomUtil.randomString(3));
      msg.setMsgDetailsUrl(rurl);

      msg.setReceivePerson(faker.name().name());
      msg.setMsgTag(faker.job().keySkills());
      msg.setMsgTitle(faker.weather().description());

      msg.setTemplateId(RandomUtil.randomEle(SendMsgHelpers.tplRange).getId());
      if (msg.getTemplateId() == 2) {
        msg.setMsgContent(StrUtil.format("{\"content\":\"{}\"}", faker.movie().quote()));
      }
      if (msg.getTemplateId() == 3) {
        msg.setMsgContent(StrUtil.format("{\"title\":\"{}\",\"content\":\"{}\"}", faker.hobbit().location(), faker.house().room()));
      }
      if (msg.getTemplateId() == 4) {
        msg.setMsgContent(StrUtil.format("{\"title\":\"{}\",\"content\":\"{}\", \"cc\":\"{}\"}", faker.hobbit().location(), faker.house().room(), faker.friends().quote()));
      }

      rc.response().putHeader("content-type", "application/json;charset=utf-8");
      rc.end(JSONUtil.toJsonStr(msg));
    });
  }

  @Override
  public void stop(Promise<Void> stopPromise) throws Exception {
    super.stop(stopPromise);
  }
}
