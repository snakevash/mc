package org.msgcenter.mc;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import org.msgcenter.mc.actor.*;

public class MainVerticle extends AbstractVerticle {

  public static void main(String[] args) {
    Vertx v = Vertx.vertx();
    init(v);
  }

  public static void init(Vertx v) {
    // 消息
    v.deployVerticle(MsgHttpReqPipelineActor.class, new DeploymentOptions());

    // db 存储消息
    v.deployVerticle(MsgDbActor.class, new DeploymentOptions().setWorker(true));
    // 分发器
    v.deployVerticle(MsgDispatcherActor.class, new DeploymentOptions().setWorker(true));
    // redis去重
    v.deployVerticle(MsgRedisActor.class, new DeploymentOptions().setWorker(true));
    // 模板处理
    v.deployVerticle(MsgTemplateActor.class, new DeploymentOptions().setWorker(true));

    // 站点消息处理器
    v.deployVerticle(MsgToSiteActor.class, new DeploymentOptions().setWorker(true));
    // 企业消息
    v.deployVerticle(MsgToCompanyWechatActor.class, new DeploymentOptions().setWorker(true));

    // 消息随机生成器
    v.deployVerticle(MsgRandomActor.class, new DeploymentOptions().setWorker(true));
    // 消息搜索
    v.deployVerticle(MsgHttpSearchActor.class, new DeploymentOptions().setWorker(true));
  }

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    Vertx v = vertx;
    init(v);
  }
}
