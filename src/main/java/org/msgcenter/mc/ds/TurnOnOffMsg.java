package org.msgcenter.mc.ds;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnOnOffMsg {

  // 渠道类型 三位格式 1xx站内信 2xx企业微信 3xx短信 4xx邮件 5xx微信公众号
  private Integer channelType;

  // 发送时间
  private String actorTime;

  // 状态
  private Boolean status;
}
