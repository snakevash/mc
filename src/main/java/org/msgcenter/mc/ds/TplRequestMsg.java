package org.msgcenter.mc.ds;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TplRequestMsg {

  private Integer id;

  private String tplCode;

  private String tplContent;
}
