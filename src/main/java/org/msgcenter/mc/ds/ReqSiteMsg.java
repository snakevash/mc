package org.msgcenter.mc.ds;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqSiteMsg {

  // 消息来源 1uc 2oa 3crm
  private Byte msgFrom;

  // 消息类型 0默认 1待办 2预警 3通知 4日程
  private Byte msgType;

  // 消息优先级 0默认 2一般 4较优先 6优先
  private Byte msgPriority;

  // 推送类型 0实时推送 1定时推送
  private Byte pushType;

  // 消息接收id
  private String receiveManId;

  // 提醒方式 0默认 1红点 2弹窗 3通知栏
  private Byte tipType;

  // 消息模板id
  private Integer templateId;

  // 定时发送时间可读
  private String pushTimeStr;

  // 发送时间
  private Integer pushTime;

  // 消息详情跳转路径
  private String msgDetailsUrl;

  // 消息接收人
  private String receivePerson;

  // 消息标签
  private String msgTag;

  // 消息标题
  private String msgTitle;

  // 消息内容
  private String msgContent;
}
