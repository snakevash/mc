package org.msgcenter.mc.ds;

import java.util.List;

public class SendMsgHelpers {

  public static Byte[] msgFromRange = {1, 2, 3};

  public static Byte[] msgTypeRange = {0, 1, 2, 3, 4};

  public static Byte[] msgPriorityRange = {0, 2, 4, 6};

  public static Byte[] pushTypeRange = {0, 1};

  public static Byte[] tipTypeRange = {0, 1, 2, 3};

  public static String ERROR_MSG_FROM_RANGE = "消息来源不在范围中";

  public static String ERROR_MSG_TYPE_RANGE = "消息类型不在范围中";

  public static String ERROR_MSG_PRIORITY_RANGE = "消息优先级不在范围中";

  public static String ERROR_PUSH_TYPE_RANGE = "消息推送不在范围中";

  public static String ERROR_TIP_TYPE_RANGE = "提示类型不在范围中";

  public static List<TplDto> tplRange = List.of(
    new TplDto(2, "SITE_1010001", "{\"msgtype\":\"text\",\"text\":{\"content\":\"${content}\"}}", 0L),
    new TplDto(3, "SITE_1010002", "{\"title\":\"${title}\",\"content\":\"${content}\"}", 0L),
    new TplDto(4, "SITE_1010003", "{\"title\":\"${title}\",\"content\":\"${content}\", \"cc\":\"${cc}\"}", 0L)
  );

  public static List<TplDto> cwTplRange = List.of(
    new TplDto(5, "CW_1010001", "{\"content\":\"${content}\"}", 0L)
  );

  // 基础站内信
  public static Integer BASE_SITE_MSG = 100;

  // 基础企业微信
  public static Integer BASE_COMPANY_WECHAT_MSG = 200;

  // 基础邮件
  public static Integer BASE_MAIL_MSG = 400;

  // 基础公众号
  public static Integer BASE_WECHAT_MSG = 500;

}
