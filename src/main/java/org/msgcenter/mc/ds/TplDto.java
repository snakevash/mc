package org.msgcenter.mc.ds;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TplDto {

  private Integer id;

  private String tplCode;

  private String tplContent;

  private Long addTime;
}
