package org.msgcenter.mc.ds;

import lombok.Data;

@Data
public class SiteMsg {
  // 消息唯一id
  private Long msgId;

  // 消息接收人
  private String receivePerson;

  // 消息详情跳转路径
  private String msgDetailsUrl;

  // 消息标签
  private String msgTag;

  // 消息标题
  private String msgTitle;

  // 消息内容
  private String msgContent;

  // 发送时间可读
  private String pushTimeStr;

  // 发送时间
  private Long pushTime;

  // 提醒方式
  private Byte tipType;
}
